import axios from 'axios';
export const state = () => ({
    authToken: null,
    isAuthenticated: false
})

export const mutations = {
    setToken(state, _token){
        state.authToken = _token;
    }
}

export const actions = {
    listOrders(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    //'Content-Type' : 'application/json',
                    //'Access-Control-Allow-Origin': 'http://localhost:8080'
                }
            };
            let formData = new FormData()
            formData.append('customer_id', data.customer_id)
            formData.append('startDate', data.startDate)
            formData.append('endDate', data.endDate)
            axios.post(process.env.apiUrl + 'order/listOrders', formData , config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    },
    
    
}

export const getters = {
    getAuthToken: state => {
        return state.authToken;
    },
    isAuthenticated: state => {
        return state.isAuthenticated;
    }
}

export const strict = false