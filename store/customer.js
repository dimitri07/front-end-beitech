import axios from 'axios';
export const state = () => ({
    authToken: null,
    isAuthenticated: false
})

export const mutations = {
    setToken(state, _token){
        state.authToken = _token;
    }
}

export const actions = {
    getCustomers(vuexContext, data){
        return new Promise((resolve, reject) => {
            let config = {
                headers: {
                    //'Content-Type' : 'application/json',
                    //'Access-Control-Allow-Origin': 'http://localhost:8080'
                }
            };
            axios.post(process.env.apiUrl + 'customer/getAllCustomer', new FormData() , config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    reject();
                })
        })
    },
    
    
}

export const getters = {
    getAuthToken: state => {
        return state.authToken;
    },
    isAuthenticated: state => {
        return state.isAuthenticated;
    }
}

export const strict = false